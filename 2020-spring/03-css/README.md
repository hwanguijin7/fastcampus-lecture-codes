# 챕터 3. 웹 사이트 색칠하기

* [01. CSS 소개와 사용법](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/03-css/01/README.md)
* [02. CSS 선택자](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/03-css/02/README.md)
* [03. CSS 기본 스타일 1](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/03-css/03/README.md)
* [04. CSS 기본 스타일 2](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/03-css/04/README.md)
* [05. CSS 레이아웃](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/03-css/05/README.md)
* [06. CSS grid 레이아웃](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/03-css/06/README.md)
* [07. CSS Material Design](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/03-css/07/README.md)
* [08. CSS Reset & 실습과제](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/03-css/08/README.md)