# 백엔드 개발자 로드맵

## 📚 컴퓨터공학 지식
> 평생 꾸준히 공부해나가야 할 부분

* Operating System
* 메모리 관리
* Process & Thread
* 네트워크

<br>

***

<br>


## 📚 리눅스
![linux](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/03/images/01.png) 

* 자유 소프트웨어 - 무료로 사용 가능한 OS
* 개발자 친화적
* 대부분의 서버에 리눅스가 사용됨

<br>

### 🔗 <a href="https://ubuntu.com/" target="_blank">리눅스 우분투</a>로 시작해보기

<br>

***

<br>

## 📚 언어 & 프레임워크


### <a href="https://www.java.com/ko/" target="_blank">Java</a> & <a href="https://spring.io/" target="_blank">스프링</a>
![java](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/03/images/02.png) 

<br>

### <a href="https://www.python.org/" target="_blank">Python</a> & <a href="https://www.djangoproject.com/" target="_blank"> django</a> / <a href="https://flask.palletsprojects.com/en/1.1.x/" target="_blank">Flask</a>
![python](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/03/images/03.png) 

<br>

### <a href="https://www.php.net/" target="_blank">PHP</a> & <a href="https://laravel.com/" target="_blank">Laravel</a> / <a href="https://codeigniter.com/" target="_blank">CodeIgniter</a>
![php](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/03/images/04.png) 

<br>

### <a href="https://www.ruby-lang.org/en/" target="_blank">Ruby</a> & <a href="https://rubyonrails.org/" target="_blank">Ruby on Rails</a>
![ruby](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/03/images/05.png) 

<br>

### <a href="https://docs.microsoft.com/en-us/dotnet/csharp/" target="_blank">C#</a> & <a href="https://dotnet.microsoft.com/download" target="_blank">.Net Core</a>
![c#](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/03/images/06.png) 

<br>

### <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank">JavaScript</a> & <a href="https://expressjs.com/" target="_blank">Express</a>
![js](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/03/images/07.png) 

<br>

### ⭐️ 언어들과 함께 공부할 것
* **`객체지향`** 프로그래밍
* **`함수형`** 프로그래밍

<br>

***

<br>

## 📚 데이터베이스

| idx | user_id | user_name | file_name | description | likes |
| :-- | :-- | :-- | :-- | :-- | :-- |
| 1 | js-master | 연놀부 | photo_1.jpg | 새로 한 벽지 | 1 |
| 2 | googler | 심청 | photo_2.jpg | 뒷마당 나무랑 뒷동산 | 3 |
| 3 | mozilla | 김삿갓 | photo_3.jpg | 수질 대박 | 2 |

<br>

```javascript
[
  {
    idx: 1,
    user_id: 'js-master',
    user_name: '연놀부',
    file_name: 'photo_1.jpg',
    description: '새로 한 벽지',
    likes: 1,
  },
  {
    idx: 2,
    user_id: 'googler',
    user_name: '심청',
    file_name: 'photo_2.jpg',
    description: '뒷마당 나무랑 뒷동산',
    likes: 3,
  },
  {
    idx: 3,
    user_id: 'mozilla',
    user_name: '김삿갓',
    file_name: 'photo_3.jpg',
    description: '수질 대박',
    likes: 2,
  }
]
```

<br>

### 🔗 <a href="https://www.mysql.com/" target="_blank">MySQL</a>로 시작해보기

![mysql](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/03/images/08.png) 