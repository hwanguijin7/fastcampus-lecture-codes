# 프로그래밍 첫걸음

* [챕터 2. 웹 사이트 뼈대 만들기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/02-html/README.md)
* [챕터 3. 웹 사이트 색칠하기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/03-css/README.md)
* [챕터 4. 나만의 사진 포트폴리오 만들기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/README.md)
* [챕터 5. 서버에 사진과 정보 저장하기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/05-firebase/README.md)
* [챕터 6. 웹 개발자 로드맵](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/06-roadmap/README.md)